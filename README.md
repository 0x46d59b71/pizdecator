# THIS PIECE OF SOFTWARE IS NO LONGER DEVELOPED

messmaker is a script that applies random styles on every element in
the container.

## How to use

```
import * as Messmaker from "./messmaker.js";

const mess = new Messmaker.Mess(/*some container like document.body*/);

mess.run();

...

mess.pause();
```

Also see `example.html`.
